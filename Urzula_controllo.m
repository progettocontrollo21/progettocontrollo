% pulizia dell'ambiente di lavoro MATLAB
clc; % pulisce la Command Window
clear; % pulisce il Workspace
close all; % chiude tutte le finestre/figure aperte

% definizione dei parametri del modello
alpha = 5.643; %rate di transizione da cellule proliferanti a cellule quiescenti
beta = 0.48; %rate di transizione da cellule quiescenti a cellule proliferanti
gam = 1.47; %rate di proliferazione cellulare
delta = 0; %rate di morte naturale delle cellule proliferanti
rho = 0.164; %rate di passaggio dal midollo nel flusso sanguigno
s = 1;
f = log(2);
g = 4*f;
h = f+g;
q1 = 1;
q2 = 1;
r1 = 1;
r2 = 1;
b=1;

% DOBBIAMO IMPLEMENTARE U (temporaneamente messo a 1) DALL'INDICE DI COSTO E DALLA SWITCHING FUNCTION
%syms u;
%u=[0;1]; 
u=1;

timerange=[0;10]; %tempo di simulazione
IC = [1;0;0]; %condizioni iniziali 

% risoluzione sistema di equazioni differenziali del modello di Panetta e Fister con ode45
[t,y] = ode45(@(t,y) ursula(t,y,alpha,beta,gam,delta,rho,f,g,h,s,u), timerange, IC);
P = y(:,1);
Q = y(:,2);
c = y(:,3);

% plot degli andamenti nel tempo di P, Q e c
figure
plot(t,y(:,1),'-b',t,y(:,2),'-r',t,y(:,3),'-g')
title('Concentrazione cellule P e Q e farmaco c');
xlabel('t');
ylabel('P, Q, c');
legend('P','Q', 'c');
grid;

% INDICE DI COSTO: definizione e massimizzazione tramite fmincon
integrando = @(t) (q1*P+q2*Q+b*u);
J = r1*P+r2*Q+integral(integrando, timerange(1), timerange(end),'ArrayValued',true); %ArrayValued is true perchè integrando restituisce un array

% risoluzione sistema 6 equazioni differenziali in 6 incognite
IC2 = [1, 0, 0, 1, 1, 0];
[t2,y2] = ode45(@(t2,y2) controllo(t2,y2,alpha,beta,gam,delta,rho,f,g,h,s,q1,q2,u), timerange, IC2);
lambda1 = y2(:,4);
lambda2 = y2(:,5);
mi = y2(:,6);

% plot secondo
figure
plot(t2,y2(:,4),'-b',t2,y2(:,5),'-r',t2,y2(:,6),'-g')
title('Costato');
xlabel('t');
ylabel('lamba1, lambda2, mi');
legend('lambda1','lambda2', 'mi');
grid;

%H = qN + b*u + lambda1*((gam-delta-alpha-s*u)*P+beta*Q) + lambda2*(alpha*P-(rho+beta)*Q) + mi*(-(f+g*u)*c+h*u); % Hamiltoniana
%fi = b + mi(h-g*c); % switching function dall'applicazione del Principio di Pontryagin
% u vale 1 quando fi>0 e vale 0 quando fi<0

%function dydt2 = boh(~,y,alpha,beta,gam,delta,rho,f,g,h,s,u)  
    %H = qN + b*u + lambda(1)*[(gam-delta-alpha-s*u)*P+beta*Q] + lambda(2)*[alpha*P-(rho+beta)*Q] + mi*[-(f+g*u)*c+h*u]
    %dydt2 = [H]
    %end

function dydt = ursula(~,y,alpha,beta,gam,delta,rho,f,g,h,s,u)  

    dydt = [(gam-delta-alpha-s*y(3))*y(1)+beta*y(2) ; alpha*y(1)-(rho+beta)*y(2) ; -(f+u*g)*y(3)+h*u]; % equazioni modello
end

function dydt2 = controllo(~,y,alpha,beta,gam,delta,rho,f,g,h,s,q1,q2,u)
    dydt2 = [(gam-delta-alpha-s*y(3))*y(1)+beta*y(2) ; alpha*y(1)-(rho+beta)*y(2) ; -(f+u*g)*y(3)+h*u ; 
        -(y(4)*(gam-delta-alpha-s*u)+y(5)*alpha+q1) ; -(y(4)*beta+y(5)*(rho+beta)+q2) ; y(6)*(f+g*u)]; % il vettore y conterrà le 6 incognite (P, Q, c,lambda(1),lambda(2) e mi)
end
