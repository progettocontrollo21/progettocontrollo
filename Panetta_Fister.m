% pulizia dell'ambiente di lavoro MATLAB
clc; % pulisce la Command Window
clear; % pulisce il Workspace
close all; % chiude tutte le finestre/figure aperte

%% MODELLO
%%

% definizione dei parametri del modello
alpha = 5.643; %rate di transizione da cellule proliferanti a cellule quiescenti
beta = 0.48; %rate di transizione da cellule quiescenti a cellule proliferanti
gam = 1.47; %rate di proliferazione cellulare
delta = 0; %rate di morte naturale delle cellule proliferanti
rho = 0.164; %rate di passaggio dal midollo nel flusso sanguigno
%a = ?; % legati alla distruzione della massa del midollo osseo
%b = ?;

timerange=[0 10]; %tempo di simulazione
IC = [0;1]; %condizioni iniziali 


% risoluzione sistema di equazioni differenziali del modello di Panetta e Fister con ode45 di MATLAB
[t,y] = ode45(@(t,y) modello(t,y,alpha,beta,gam,delta,rho), timerange, IC);

% plot degli andamenti nel tempo di P e Q
plot(t,y(:,1),'-b',t,y(:,2),'-r')
title('Concentrazione cellule P e Q');
xlabel('t');
ylabel('P e Q');
legend('P','Q');
grid;

function dydt = modello(~,y,alpha,beta,gam,delta,rho) %la ~ sta al posto di t perché altrimenti c'è il warning che avverte 
                                                      %che la t non è
                                                      %utilizzata nel corpo
        dydt = [(gam-delta-alpha)*y(1)+beta*y(2) ; alpha*y(1)-(rho+beta)*y(2)]; %sistema di equazioni del modello
end
